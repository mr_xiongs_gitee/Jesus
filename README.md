# Jesus（耶稣）

## 仓库：
https://gitee.com/mr_xiongs_gitee/Jesus

## 技术栈

SpringBoot2 + Vue2 + ElementUI + Axios + Hutool + Mysql + Echarts

## 项目介绍
本项目适用于小白，教你从零开始搭建一个springboot + vue的管理项目；

本项目可能过于简单，后期会参考若依来进行更加完善的封装

## 项目部署
1、后台
> 使用git拉取代码后给项目添加maven，等待依赖下载完毕；
> 创建数据库jesus-vue并导入数据脚本（在db文件夹中）
> 打开项目运行JesusApplication.java

2、前台
> 在IDEA中打开vue-ui文件，右击open in terminal
> npm install   #安装依赖
> npm run server

## 项目结构
```
Jesus
├─db                            数据库SQL脚本
│ 
├─Jesus-common                  公共模块
│    │ 
│    └─java 
│         ├─constants           常量类
│         ├─domain              通用类
│         ├─dto                 DTO
│         ├─excel               Excel导出
│         ├─interceptor         拦截器
│         ├─enums               通用枚举
│         └─util                工具类
│   
├─Jesus-web                     系统管理
│    │ 
│    ├─java 
│    │    ├─config              配置类
│    │    ├─controller          controller类
│    │    ├─domain              基础通用类
│    │    ├─entity              实体类
│    │    ├─exception           全局异常处理
│    │    ├─mapper              mapper
│    │    ├─service             实现类
│    │    └─controller          controller类
│    └─resource
│        ├─mappers              mapper文件
│        ├─templates            模板文件
│        ├─application.yml      全局配置文件
│        ├─banner.txt           启动图
│        └─logback.xml          日志配置文件
│
└─vue-ui                        前端代码（vue + element ui）
     
```
