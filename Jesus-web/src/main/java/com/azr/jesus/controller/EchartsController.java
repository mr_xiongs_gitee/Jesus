package com.azr.jesus.controller;

import cn.hutool.core.collection.CollUtil;
import com.azr.jesus.common.domain.ResultData;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/echarts")
public class EchartsController {


    @GetMapping("/example")
    public ResultData get() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("x", CollUtil.newArrayList("Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"));
        map.put("y", CollUtil.newArrayList(820, 932, 901, 934, 1290, 1330, 1320));
        return ResultData.success(map);
    }
}
