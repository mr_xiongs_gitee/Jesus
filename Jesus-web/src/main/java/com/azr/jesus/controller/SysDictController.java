package com.azr.jesus.controller;

import com.azr.jesus.common.domain.ResultData;
import com.azr.jesus.common.utils.EasyExcelUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import com.azr.jesus.service.ISysDictService;
import com.azr.jesus.entity.SysDictType;

import java.io.IOException;
import java.util.Date;
import java.util.List;


/**
 * 字典类型表 前端前端控制器 
 * @author: Xiong
 * @date: 2022-12-01
 */
@RestController
@RequestMapping("/sys-dict")
public class SysDictController {

    @Resource
    private ISysDictService sysDictService;

    @PostMapping
    @ApiOperation(value = "新增")
    public ResultData save(@RequestBody SysDictType sysDictType) {
        sysDictType.setCreateTime(new Date());
        return ResultData.success(sysDictService.saveOrUpdate(sysDictType));
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除")
    public ResultData delete(@PathVariable Integer id) {
        return ResultData.success(sysDictService.removeById(id));
    }

    @PostMapping(value = "/batchDelete")
    @ApiOperation(value = "批量删除")
    public ResultData batchDelete(@RequestBody List<Integer> ids) {
        return ResultData.success(sysDictService.removeBatchByIds(ids));
    }

    @PostMapping(value = "/update")
    @ApiOperation(value = "修改")
    public ResultData update(@RequestBody SysDictType sysDictType) {
        sysDictType.setUpdateTime(new Date());
        return ResultData.success(sysDictService.updateById(sysDictType));
    }

    @GetMapping
    @ApiOperation(value = "获取列表")
    public ResultData findAll() {
        return ResultData.success(sysDictService.list());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "获取一条")
    public ResultData findOne(@PathVariable Integer id) {
        return ResultData.success(sysDictService.getById(id));
    }

    @GetMapping("/page")
    @ApiOperation(value = "分页查询")
    public ResultData findPage(@RequestParam Integer pageNum,
                                @RequestParam Integer pageSize) {
        QueryWrapper<SysDictType> query = new QueryWrapper<>();
        return ResultData.success(sysDictService.page(new Page<>(pageNum, pageSize), query));
    }

    @GetMapping("/export")
    @ApiOperation(value = "Excel导出")
    public void export(HttpServletResponse response) throws IOException {
        List<SysDictType> sysDictTypeList = sysDictService.list();
        EasyExcelUtil.download(response, sysDictTypeList, "字典类型表");
    }
}

