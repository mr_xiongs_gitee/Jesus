package com.azr.jesus.controller;

import cn.hutool.core.util.StrUtil;
import com.azr.jesus.common.domain.ResultData;
import com.azr.jesus.common.utils.EasyExcelUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import com.azr.jesus.service.ISysRoleService;
import com.azr.jesus.entity.SysRole;


/**
 * 角色信息表 前端控制器
 * @author: Xiong
 * @date: 2022-11-20
 */
@RestController
@RequestMapping("/sys-role")
public class SysRoleController {

    @Resource
    private ISysRoleService sysRoleService;

    @PostMapping
    @ApiOperation(value = "新增")
    public ResultData save(@RequestBody SysRole sysRole) {
        sysRole.setCreateTime(new Date());
        return ResultData.success(sysRoleService.saveOrUpdate(sysRole));
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除")
    public ResultData delete(@PathVariable Integer id) {
        return ResultData.success(sysRoleService.removeById(id));
    }

    @PostMapping(value = "/batchDelete")
    @ApiOperation(value = "批量删除")
    public ResultData batchDelete(@RequestBody List<Integer> ids) {
        return ResultData.success(sysRoleService.removeBatchByIds(ids));
    }

    @PostMapping(value = "/update")
    @ApiOperation(value = "修改")
    public ResultData update(@RequestBody SysRole sysRole) {
        sysRole.setUpdateTime(new Date());
        return ResultData.success(sysRoleService.updateById(sysRole));
    }

    @GetMapping
    @ApiOperation(value = "获取列表")
    public ResultData findAll() {
        return ResultData.success(sysRoleService.list());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "获取一条")
    public ResultData findOne(@PathVariable Integer id) {
        return ResultData.success(sysRoleService.getById(id));
    }

    @GetMapping("/page")
    @ApiOperation(value = "分页查询")
    public ResultData findPage(@RequestParam Integer pageNum,
                               @RequestParam Integer pageSize,
                               @RequestParam String roleName,
                               @RequestParam String roleKey,
                               @RequestParam String status
    ) {
        QueryWrapper<SysRole> query = new QueryWrapper<>();
        if (StrUtil.isNotBlank(roleName)) {
            query.like("role_name", roleName);
        }
        if (StrUtil.isNotBlank(roleKey)) {
            query.eq("role_key", roleKey);
        }
        if (StrUtil.isNotBlank(status)) {
            query.eq("status", status);
        }
        query.orderByAsc("role_sort");
        return ResultData.success(sysRoleService.page(new Page<>(pageNum, pageSize), query));
    }

    @GetMapping("/export")
    @ApiOperation(value = "Excel导出")
    public void export(HttpServletResponse response) throws IOException {
        List<SysRole> sysRoleList = sysRoleService.list();
        EasyExcelUtil.download(response, sysRoleList, "角色信息表");
    }

    /**
     * 绑定角色和菜单的关系
     * @param roleId 角色id
     * @param menuIds 菜单id数组
     * @return
     */
    @PostMapping("/roleMenu/{roleId}")
    @ApiOperation(value = "绑定角色和菜单的关系")
    public ResultData roleMenu(@PathVariable Long roleId, @RequestBody List<Long> menuIds) {
        sysRoleService.setRoleMenu(roleId, menuIds);
        return ResultData.success();
    }

    @GetMapping("/roleMenu/{roleId}")
    @ApiOperation(value = "获取角色和菜单的关系")
    public ResultData getRoleMenu(@PathVariable Long roleId) {
        return ResultData.success( sysRoleService.getRoleMenu(roleId));
    }
}

