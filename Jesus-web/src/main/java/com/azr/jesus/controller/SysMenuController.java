package com.azr.jesus.controller;

import com.azr.jesus.common.constants.DictConstants;
import com.azr.jesus.common.domain.ResultData;
import com.azr.jesus.common.utils.EasyExcelUtil;
import com.azr.jesus.entity.Dict;
import com.azr.jesus.mapper.SysDictMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import com.azr.jesus.service.ISysMenuService;
import com.azr.jesus.entity.SysMenu;


/**
 * 菜单权限表 前端控制器
 * @author: Xiong
 * @date: 2022-11-28
 */
@RestController
@RequestMapping("/sys-menu")
public class SysMenuController {

    @Resource
    private ISysMenuService sysMenuService;

    @Resource
    private SysDictMapper dictMapper;

    @PostMapping
    @ApiOperation(value = "新增")
    public ResultData save(@RequestBody SysMenu sysMenu) {
        sysMenu.setCreateTime(new Date());
        return ResultData.success(sysMenuService.saveOrUpdate(sysMenu));
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除")
    public ResultData delete(@PathVariable Integer id) {
        return ResultData.success(sysMenuService.removeById(id));
    }

    @PostMapping(value = "/batchDelete")
    @ApiOperation(value = "批量删除")
    public ResultData batchDelete(@RequestBody List<Integer> ids) {
        return ResultData.success(sysMenuService.removeBatchByIds(ids));
    }

    @PostMapping(value = "/update")
    @ApiOperation(value = "修改")
    public ResultData update(@RequestBody SysMenu sysMenu) {
        sysMenu.setUpdateTime(new Date());
        return ResultData.success(sysMenuService.updateById(sysMenu));
    }

    @GetMapping
    @ApiOperation(value = "获取列表")
    public ResultData findAll(@RequestParam(defaultValue = "") String menuName) {
        return ResultData.success(sysMenuService.findMenus(menuName));
    }

    @GetMapping("/menuIds")
    public ResultData findAllMenuIds() {
        return ResultData.success(sysMenuService.list().stream().map(SysMenu::getMenuId));
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "获取一条")
    public ResultData findOne(@PathVariable Integer id) {
        return ResultData.success(sysMenuService.getById(id));
    }

    @GetMapping("/page")
    @ApiOperation(value = "分页查询")
    public ResultData findPage(@RequestParam Integer pageNum,
                                @RequestParam Integer pageSize) {
        QueryWrapper<SysMenu> query = new QueryWrapper<>();
        return ResultData.success(sysMenuService.page(new Page<>(pageNum, pageSize), query));
    }

    @GetMapping("/export")
    @ApiOperation(value = "Excel导出")
    public void export(HttpServletResponse response) throws IOException {
        List<SysMenu> sysMenuList = sysMenuService.list();
        EasyExcelUtil.download(response, sysMenuList, "菜单信息表");
    }

    @GetMapping("/icons")
    public ResultData getIcons() {
        QueryWrapper<Dict> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("type", DictConstants.DICT_TYPE_ICON);
        return ResultData.success(dictMapper.selectList(queryWrapper));
    }
}

