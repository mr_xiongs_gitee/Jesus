package com.azr.jesus.controller;

import com.azr.jesus.common.domain.ResultData;
import com.azr.jesus.common.utils.EasyExcelUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Date;

import com.azr.jesus.service.ISysDeptService;
import com.azr.jesus.entity.SysDept;


/**
 * 部门表 前端前端控制器 
 * @author: Xiong
 * @date: 2022-12-01
 */
@RestController
@RequestMapping("/sys-dept")
public class SysDeptController {

    @Resource
    private ISysDeptService sysDeptService;

    @PostMapping
    @ApiOperation(value = "新增")
    public ResultData save(@RequestBody SysDept sysDept) {
        sysDept.setCreateTime(new Date());
        return ResultData.success(sysDeptService.saveOrUpdate(sysDept));
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除")
    public ResultData delete(@PathVariable Integer id) {
        return ResultData.success(sysDeptService.removeById(id));
    }

    @PostMapping(value = "/batchDelete")
    @ApiOperation(value = "批量删除")
    public ResultData batchDelete(@RequestBody List<Integer> ids) {
        return ResultData.success(sysDeptService.removeBatchByIds(ids));
    }

    @PostMapping(value = "/update")
    @ApiOperation(value = "修改")
    public ResultData update(@RequestBody SysDept sysDept) {
        sysDept.setUpdateTime(new Date());
        return ResultData.success(sysDeptService.updateById(sysDept));
    }

    @GetMapping
    @ApiOperation(value = "获取列表")
    public ResultData findAll(@RequestParam(defaultValue = "") String deptName) {
        return ResultData.success(sysDeptService.findDeptList(deptName));
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "获取一条")
    public ResultData findOne(@PathVariable Integer id) {
        return ResultData.success(sysDeptService.getById(id));
    }

    @GetMapping("/page")
    @ApiOperation(value = "分页查询")
    public ResultData findPage(@RequestParam Integer pageNum,
                                @RequestParam Integer pageSize) {
        QueryWrapper<SysDept> query = new QueryWrapper<>();
        return ResultData.success(sysDeptService.page(new Page<>(pageNum, pageSize), query));
    }

    @GetMapping("/export")
    @ApiOperation(value = "Excel导出")
    public void export(HttpServletResponse response) throws IOException {
        List<SysDept> sysDeptList = sysDeptService.list();
        EasyExcelUtil.download(response, sysDeptList, "部门信息表");
    }
}

