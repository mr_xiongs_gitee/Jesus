package com.azr.jesus.controller;

import cn.hutool.core.util.StrUtil;
import com.azr.jesus.common.domain.ResultData;
import com.azr.jesus.common.jwt.TokenUtils;
import com.azr.jesus.common.utils.EasyExcelUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import com.azr.jesus.service.ISysArticleService;
import com.azr.jesus.entity.SysArticle;


/**
 * 系统文章表 前端控制器
 * @author: Xiong
 * @date: 2022-12-19
 */
@RestController
@RequestMapping("/sys-article")
public class SysArticleController {

    @Resource
    private ISysArticleService sysArticleService;

    @PostMapping
    @ApiOperation(value = "新增")
    public ResultData save(@RequestBody SysArticle sysArticle) {
        sysArticle.setCreateTime(new Date());
        sysArticle.setUser(TokenUtils.getCurrentUser().getNickName());
        return ResultData.success(sysArticleService.saveOrUpdate(sysArticle));
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除")
    public ResultData delete(@PathVariable Integer id) {
        return ResultData.success(sysArticleService.removeById(id));
    }

    @PostMapping(value = "/batchDelete")
    @ApiOperation(value = "批量删除")
    public ResultData batchDelete(@RequestBody List<Integer> ids) {
        return ResultData.success(sysArticleService.removeBatchByIds(ids));
    }

    @PostMapping(value = "/update")
    @ApiOperation(value = "修改")
    public ResultData update(@RequestBody SysArticle sysArticle) {
        return ResultData.success(sysArticleService.updateById(sysArticle));
    }

    @GetMapping
    @ApiOperation(value = "获取列表")
    public ResultData findAll() {
        return ResultData.success(sysArticleService.list());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "获取一条")
    public ResultData findOne(@PathVariable Integer id) {
        return ResultData.success(sysArticleService.getById(id));
    }

    @GetMapping("/page")
    @ApiOperation(value = "分页查询")
    public ResultData findPage(@RequestParam Integer pageNum,
                                @RequestParam Integer pageSize) {
        QueryWrapper<SysArticle> query = new QueryWrapper<>();
        return ResultData.success(sysArticleService.page(new Page<>(pageNum, pageSize), query));
    }

    @GetMapping("/export")
    @ApiOperation(value = "Excel导出")
    public void export(HttpServletResponse response) throws IOException {
        List<SysArticle> sysArticleList = sysArticleService.list();
        EasyExcelUtil.download(response, sysArticleList, "系统文章表");
    }
}

