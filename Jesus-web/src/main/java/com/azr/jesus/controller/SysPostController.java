package com.azr.jesus.controller;

import cn.hutool.core.util.StrUtil;
import com.azr.jesus.common.domain.ResultData;
import com.azr.jesus.common.utils.EasyExcelUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Date;

import com.azr.jesus.service.ISysPostService;
import com.azr.jesus.entity.SysPost;


/**
 * 岗位信息表 前端控制器
 * @author: Xiong
 * @date: 2022-12-01
 */
@RestController
@RequestMapping("/sys-post")
public class SysPostController {

    @Resource
    private ISysPostService sysPostService;

    @PostMapping
    @ApiOperation(value = "新增")
    public ResultData save(@RequestBody SysPost sysPost) {
        sysPost.setCreateTime(new Date());
        return ResultData.success(sysPostService.saveOrUpdate(sysPost));
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除")
    public ResultData delete(@PathVariable Integer id) {
        return ResultData.success(sysPostService.removeById(id));
    }

    @PostMapping(value = "/batchDelete")
    @ApiOperation(value = "批量删除")
    public ResultData batchDelete(@RequestBody List<Integer> ids) {
        return ResultData.success(sysPostService.removeBatchByIds(ids));
    }

    @PostMapping(value = "/update")
    @ApiOperation(value = "修改")
    public ResultData update(@RequestBody SysPost sysPost) {
        sysPost.setUpdateTime(new Date());
        return ResultData.success(sysPostService.updateById(sysPost));
    }

    @GetMapping
    @ApiOperation(value = "获取列表")
    public ResultData findAll() {
        return ResultData.success(sysPostService.list());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "获取一条")
    public ResultData findOne(@PathVariable Integer id) {
        return ResultData.success(sysPostService.getById(id));
    }

    @GetMapping("/page")
    @ApiOperation(value = "分页查询")
    public ResultData findPage(@RequestParam Integer pageNum,
                               @RequestParam Integer pageSize,
                               @RequestParam String postName,
                               @RequestParam String postCode
    ) {
        QueryWrapper<SysPost> query = new QueryWrapper<>();
        if (StrUtil.isNotBlank(postName)) {
            query.like("post_name", postName);
        }
        if (StrUtil.isNotBlank(postCode)) {
            query.eq("post_code", postCode);
        }
        return ResultData.success(sysPostService.page(new Page<>(pageNum, pageSize), query));
    }

    @GetMapping("/export")
    @ApiOperation(value = "Excel导出")
    public void export(HttpServletResponse response) throws IOException {
        List<SysPost> sysPostList = sysPostService.list();
        EasyExcelUtil.download(response, sysPostList, "岗位信息表");
    }
}

