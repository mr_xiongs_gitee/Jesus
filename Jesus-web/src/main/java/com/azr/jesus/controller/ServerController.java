package com.azr.jesus.controller;

import com.azr.jesus.common.domain.ResultData;
import com.azr.jesus.entity.Server;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 服务监控
 * @author:: Xiong
 * @date:: 2022/12/8 10:09
 */
@RestController
@RequestMapping("/server")
public class ServerController {

    @GetMapping("/")
    @ApiOperation(value = "获取服务器相关信息")
    public ResultData getServer() {
        Server server = new Server();
        server.setValueTo();
        return ResultData.success(server);
    }
}
