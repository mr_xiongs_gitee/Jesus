package com.azr.jesus.controller;

import com.azr.jesus.common.domain.ResultData;
import com.azr.jesus.common.utils.EasyExcelUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import com.azr.jesus.service.ISysLoginInfoService;
import com.azr.jesus.entity.SysLoginInfo;


/**
 * 系统访问记录 前端控制器
 * @author: Xiong
 * @date: 2022-12-06
 */
@RestController
@RequestMapping("/sys-login-info")
public class SysLoginInfoController {

    @Resource
    private ISysLoginInfoService sysLoginInfoService;

    @PostMapping
    @ApiOperation(value = "新增")
    public ResultData save(@RequestBody SysLoginInfo sysLoginInfo) {
        return ResultData.success(sysLoginInfoService.saveOrUpdate(sysLoginInfo));
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除")
    public ResultData delete(@PathVariable Integer id) {
        return ResultData.success(sysLoginInfoService.removeById(id));
    }

    @PostMapping(value = "/batchDelete")
    @ApiOperation(value = "批量删除")
    public ResultData batchDelete(@RequestBody List<Integer> ids) {
        return ResultData.success(sysLoginInfoService.removeBatchByIds(ids));
    }

    @PostMapping(value = "/update")
    @ApiOperation(value = "修改")
    public ResultData update(@RequestBody SysLoginInfo sysLoginInfo) {
        return ResultData.success(sysLoginInfoService.updateById(sysLoginInfo));
    }

    @GetMapping
    @ApiOperation(value = "获取列表")
    public ResultData findAll() {
        return ResultData.success(sysLoginInfoService.list());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "获取一条")
    public ResultData findOne(@PathVariable Integer id) {
        return ResultData.success(sysLoginInfoService.getById(id));
    }

    @GetMapping("/page")
    @ApiOperation(value = "分页查询")
    public ResultData findPage(@RequestParam Integer pageNum,
                                @RequestParam Integer pageSize) {
        QueryWrapper<SysLoginInfo> query = new QueryWrapper<>();
        query.orderByDesc("info_id");
        return ResultData.success(sysLoginInfoService.page(new Page<>(pageNum, pageSize), query));
    }

    @GetMapping("/export")
    @ApiOperation(value = "Excel导出")
    public void export(HttpServletResponse response) throws IOException {
        List<SysLoginInfo> sysLoginInfoList = sysLoginInfoService.list();
        EasyExcelUtil.download(response, sysLoginInfoList, "系统访问记录");
    }
}

