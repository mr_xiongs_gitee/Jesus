package com.azr.jesus.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @description:
 * @author:: Xiong
 * @date:: 2022/11/28 15:26
 */
@Data
@TableName("sys_dict")
public class Dict {
    private String name;
    private String value;
    private String type;
}
