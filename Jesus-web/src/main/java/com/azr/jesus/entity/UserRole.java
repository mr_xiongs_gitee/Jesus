package com.azr.jesus.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 用户角色关系表
 * @author:: Xiong
 * @date:: 2022/11/29 15:56
 */
@Data
@TableName("sys_user_role")
public class UserRole {
    private Long userId;
    private Long roleId;
}
