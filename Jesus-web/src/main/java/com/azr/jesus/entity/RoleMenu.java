package com.azr.jesus.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 角色菜单关系表
 * @author:: Xiong
 * @date:: 2022/11/28 15:10
 */
@Data
@TableName("sys_role_menu")
public class RoleMenu {
    private Long roleId;
    private Long menuId;
}
