package com.azr.jesus.entity;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.azr.jesus.common.converter.GenderConverter;
import com.azr.jesus.common.domain.BaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @description:
 * @author:: Xiong
 * @date:: 2022/10/30 20:12
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "sys_user")
public class User extends BaseEntity{

    /**
     * 用户ID
     */
    @TableId(type = IdType.AUTO)
    @ExcelIgnore
    private Long userId;

    /**
     * 部门ID
     */
    @ExcelProperty(value = "部门ID", index = 0)
    private Long deptId;

    /**
     * 用户账号
     */
    @ExcelProperty(value = "用户账号", index = 1)
    private String userName;

    /**
     * 用户昵称
     */
    @ExcelProperty(value = "用户昵称", index = 2)
    private String nickName;

    /**
     * 用户邮箱
     */
    @ExcelProperty(value = "用户邮箱", index = 3)
    private String email;

    /**
     * 手机号码
     */
    @ExcelProperty(value = "手机号码", index = 4)
    private String phonenumber;

    /**
     * 用户性别
     */
    @ExcelProperty(value = "用户性别", index = 5, converter = GenderConverter.class)
    private String sex;

    /**
     * 用户头像
     */
    @ExcelIgnore
    private String avatar;

    /**
     * 密码
     * 前端不返回
     */
    @JsonIgnore
    @ExcelIgnore
    private String password;

    /**
     * 帐号状态（0正常 1停用）
     */
    @ExcelIgnore
    private Boolean status;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @ExcelIgnore
    private String delFlag;

    /**
     * 最后登录IP
     */
    @ExcelIgnore
    private String loginIp;

    /**
     * 最后登录时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ExcelIgnore
    private Date loginDate;

}
