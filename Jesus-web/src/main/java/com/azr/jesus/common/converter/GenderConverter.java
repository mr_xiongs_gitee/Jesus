package com.azr.jesus.common.converter;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.CellData;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.property.ExcelContentProperty;

/**
 * @description:
 * @author:: Xiong
 * @date:: 2022/12/2 17:11
 */
public class GenderConverter implements Converter<String> {

    private static final String MAN = "男";
    private static final String WOMAN = "女";
    private static final String UNKNOWN = "未知";


    @Override
    public Class<?> supportJavaTypeKey() {
        // 实体类中对象属性类型
        return Integer.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        // Excel中对应的CellData属性类型
        return CellDataTypeEnum.STRING;
    }

    @Override
    public String convertToJavaData(CellData cellData, ExcelContentProperty excelContentProperty,
                                     GlobalConfiguration globalConfiguration) {
        // 从Cell中读取数据
        String gender = cellData.getStringValue();
        // 判断Excel中的值，将其转换为预期的数值
        if (MAN.equals(gender)) {
            return "0";
        } else if (WOMAN.equals(gender)) {
            return "1";
        } else if (UNKNOWN.equals(gender)){
            return "2";
        }
        return null;
    }

    @Override
    public CellData<?> convertToExcelData(String string, ExcelContentProperty excelContentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        // 判断实体类中获取的值，转换为Excel预期的值，并封装为CellData对象
        if (string == null) {
            return new CellData<>("");
        } else if ("0".equals(string)) {
            return new CellData<>(MAN);
        } else if ("1".equals(string)) {
            return new CellData<>(WOMAN);
        } else if ("2".equals(string)) {
        }
        return new CellData<>("");
    }

}
