package com.azr.jesus.common.dto;

import com.azr.jesus.entity.SysMenu;
import lombok.Data;

import java.util.List;

/**
 * @description:
 * @author:: Xiong
 * @date:: 2022/11/13 21:21
 */
@Data
public class UserDTO {
    private String userName;
    private String password;
    private String nickName;
    private String avatar;
    private String token;
    private List<SysMenu> menus;
}
