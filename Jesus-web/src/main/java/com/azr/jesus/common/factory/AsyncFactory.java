package com.azr.jesus.common.factory;

import com.azr.jesus.common.constants.Constants;
import com.azr.jesus.common.utils.LogUtils;
import com.azr.jesus.common.utils.ServletUtils;
import com.azr.jesus.common.utils.StringUtils;
import com.azr.jesus.common.utils.ip.AddressUtils;
import com.azr.jesus.common.utils.ip.IpUtils;
import com.azr.jesus.common.utils.spring.SpringUtils;
import com.azr.jesus.entity.SysLoginInfo;
import com.azr.jesus.service.ISysLoginInfoService;
import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.xml.crypto.Data;
import java.util.Date;
import java.util.TimerTask;

/**
 * 异步工厂（产生任务用）
 *
 * @author:: Xiong
 * @date:: 2022/12/7 10:45
 */
public class AsyncFactory {
    private static final Logger sys_user_logger = LoggerFactory.getLogger("sys-user" );

    /**
     * 记录登录信息
     *
     * @param username 用户名
     * @param status   状态
     * @param message  消息
     * @param args     列表
     * @return 任务task
     */
    public static TimerTask recordLoginInfo(final String username, final String status, final String message,
                                            final Object... args) {
        final UserAgent userAgent = UserAgent.parseUserAgentString(ServletUtils.getRequest().getHeader("User-Agent" ));
        final String ip = IpUtils.getIpAddr(ServletUtils.getRequest());
        return new TimerTask() {
            @Override
            public void run() {
                String address = AddressUtils.getRealAddressByIP(ip);
                String s = LogUtils.getBlock(ip) + address + LogUtils.getBlock(username) +
                        LogUtils.getBlock(status) + LogUtils.getBlock(message);
                // 打印信息到日志
                sys_user_logger.info(s, args);
                // 获取操作系统
                String os = userAgent.getOperatingSystem().getName();
                // 获取客户端浏览器
                String browser = userAgent.getBrowser().getName();
                // 封装对象
                SysLoginInfo loginInfo = new SysLoginInfo();
                loginInfo.setUserName(username);
                loginInfo.setIpaddr(ip);
                loginInfo.setLoginLocation(address);
                loginInfo.setBrowser(browser);
                loginInfo.setOs(os);
                loginInfo.setMsg(message);
                loginInfo.setLoginTime(new Date());
                // 日志状态
                if (StringUtils.equalsAny(status, Constants.LOGIN_SUCCESS, Constants.LOGOUT, Constants.REGISTER)) {
                    loginInfo.setStatus(Boolean.TRUE);
                } else if (Constants.LOGIN_FAIL.equals(status)) {
                    loginInfo.setStatus(Boolean.FALSE);
                }
                // 插入数据
                SpringUtils.getBean(ISysLoginInfoService.class).saveOrUpdate(loginInfo);
            }
        };
    }

}
