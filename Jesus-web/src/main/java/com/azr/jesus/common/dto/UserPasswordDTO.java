package com.azr.jesus.common.dto;

import lombok.Data;

/**
 * @description:
 * @author:: Xiong
 * @date:: 2022/11/30 10:12
 */
@Data
public class UserPasswordDTO {
    private String userName;
    private String password;
    private String newPassword;
}
