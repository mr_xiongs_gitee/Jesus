package com.azr.jesus.mapper;

import com.azr.jesus.entity.SysArticle;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 系统文章表 Mapper 接口
 * @author: Xiong
 * @date: 2022-12-19
 */
public interface SysArticleMapper extends BaseMapper<SysArticle> {

}
