package com.azr.jesus.mapper;

import com.azr.jesus.entity.RoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description:
 * @author:: Xiong
 * @date:: 2022/11/28 15:10
 */
public interface RoleMenuMapper extends BaseMapper<RoleMenu> {

    List<Long> selectByRoleId(@Param("roleId") Long roleId);

    int deleteByRoleId(@Param("roleId") Long roleId);
}
