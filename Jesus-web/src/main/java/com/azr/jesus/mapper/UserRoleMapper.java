package com.azr.jesus.mapper;

import com.azr.jesus.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description:
 * @author:: Xiong
 * @date:: 2022/11/29 16:14
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

    List<Long> selectByUserId(@Param("userId") Long userId);

    void deleteByUserId(Long userId);
}
