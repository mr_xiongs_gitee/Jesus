package com.azr.jesus.mapper;

import com.azr.jesus.entity.SysLoginInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 系统访问记录 Mapper 接口
 * @author: Xiong
 * @date: 2022-12-06
 */
public interface SysLoginInfoMapper extends BaseMapper<SysLoginInfo> {

}
