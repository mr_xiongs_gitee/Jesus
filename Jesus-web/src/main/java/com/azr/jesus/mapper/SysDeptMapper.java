package com.azr.jesus.mapper;

import com.azr.jesus.entity.SysDept;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 部门表 Mapper 接口
 * @author: Xiong
 * @date: 2022-12-01
 */
public interface SysDeptMapper extends BaseMapper<SysDept> {

}
