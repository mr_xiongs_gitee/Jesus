package com.azr.jesus.mapper;

import com.azr.jesus.entity.SysDictType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 字典类型表 Mapper 接口
 * @author: Xiong
 * @date: 2022-12-01
 */
public interface SysDictTypeMapper extends BaseMapper<SysDictType> {

}
