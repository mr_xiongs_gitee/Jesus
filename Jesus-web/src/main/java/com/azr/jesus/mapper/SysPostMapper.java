package com.azr.jesus.mapper;

import com.azr.jesus.entity.SysPost;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 岗位信息表 Mapper 接口
 * @author: Xiong
 * @date: 2022-12-01
 */
public interface SysPostMapper extends BaseMapper<SysPost> {

}
