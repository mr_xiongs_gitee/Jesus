package com.azr.jesus.mapper;

import com.azr.jesus.entity.Dict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @description:
 * @author:: Xiong
 * @date:: 2022/11/28 15:37
 */
public interface SysDictMapper extends BaseMapper<Dict> {
}
