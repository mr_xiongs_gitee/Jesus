package com.azr.jesus.mapper;

import com.azr.jesus.common.dto.UserPasswordDTO;
import com.azr.jesus.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @description:
 * @author:: Xiong
 * @date:: 2022/10/30 20:12
 */
public interface UserMapper extends BaseMapper<User> {

    int updatePassword(UserPasswordDTO userPasswordDTO);

}
