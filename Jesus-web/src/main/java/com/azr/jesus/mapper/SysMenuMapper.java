package com.azr.jesus.mapper;

import com.azr.jesus.entity.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 菜单权限表 Mapper 接口
 * @author: Xiong
 * @date: 2022-11-28
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

}
