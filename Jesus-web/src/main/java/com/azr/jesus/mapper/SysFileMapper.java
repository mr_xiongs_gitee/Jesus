package com.azr.jesus.mapper;

import com.azr.jesus.entity.SysFiles;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 文件信息表 Mapper 接口
 * @author: Xiong
 * @date: 2022-11-20
 */
public interface SysFileMapper extends BaseMapper<SysFiles> {

}
