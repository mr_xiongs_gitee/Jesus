package com.azr.jesus.mapper;

import com.azr.jesus.entity.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 角色信息表 Mapper 接口
 * @author: Xiong
 * @date: 2022-11-20
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
