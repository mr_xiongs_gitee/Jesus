package com.azr.jesus.exception;

import lombok.Getter;

/**
 * @description:
 * @author:: Xiong
 * @date:: 2022/11/15 22:19
 */
@Getter
public class ServiceException extends RuntimeException {
    private final int code;

    public ServiceException(int code, String msg) {
        super(msg);
        this.code = code;
    }
}
