package com.azr.jesus.exception;

import com.azr.jesus.common.domain.ResultData;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 全局异常处理类
 * @author:: Xiong
 * @date:: 2022/11/15 22:17
 */
@ControllerAdvice
public class GlobalExceptionHandle {

    @ExceptionHandler(ServiceException.class)
    @ResponseBody
    public ResultData handle(ServiceException e){
        return ResultData.error(e.getCode(), e.getMessage());
    }
}
