package com.azr.jesus.service;

import com.azr.jesus.common.dto.UserDTO;
import com.azr.jesus.common.dto.UserPasswordDTO;
import com.azr.jesus.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 用户信息表 服务类
 * @author: Xiong
 * @date: 2022-11-08
 */
public interface IUserService extends IService<User> {

    UserDTO login(UserDTO userDto);

    User register(UserDTO userDto);

    List<Long> getRoleMenu(Long userId);

    void setUserRole(Long userId, List<Long> roleIds);

    int updatePassword(UserPasswordDTO userPasswordDTO);
}
