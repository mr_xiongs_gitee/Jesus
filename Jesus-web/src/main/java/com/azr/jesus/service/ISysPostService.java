package com.azr.jesus.service;

import com.azr.jesus.entity.SysPost;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 岗位信息表 服务类
 * @author: Xiong
 * @date: 2022-12-01
 */
public interface ISysPostService extends IService<SysPost> {

}
