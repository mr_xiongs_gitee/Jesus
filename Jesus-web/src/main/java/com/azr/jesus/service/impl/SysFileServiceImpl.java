package com.azr.jesus.service.impl;

import com.azr.jesus.entity.SysFiles;
import com.azr.jesus.mapper.SysFileMapper;
import com.azr.jesus.service.ISysFileService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 文件信息表 服务实现类
 * @author: Xiong
 * @date: 2022-11-20
 */
@Service
public class SysFileServiceImpl extends ServiceImpl<SysFileMapper, SysFiles> implements ISysFileService {

}
