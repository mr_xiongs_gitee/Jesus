package com.azr.jesus.service.impl;

import com.azr.jesus.entity.SysDictType;
import com.azr.jesus.mapper.SysDictTypeMapper;
import com.azr.jesus.service.ISysDictService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 字典类型表 服务实现类
 * @author: Xiong
 * @date: 2022-12-01
 */
@Service
public class SysDictServiceImpl extends ServiceImpl<SysDictTypeMapper, SysDictType> implements ISysDictService {

}
