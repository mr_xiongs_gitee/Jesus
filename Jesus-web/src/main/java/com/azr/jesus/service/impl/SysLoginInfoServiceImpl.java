package com.azr.jesus.service.impl;

import com.azr.jesus.entity.SysLoginInfo;
import com.azr.jesus.mapper.SysLoginInfoMapper;
import com.azr.jesus.service.ISysLoginInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 系统访问记录 服务实现类
 * @author: Xiong
 * @date: 2022-12-06
 */
@Service
public class SysLoginInfoServiceImpl extends ServiceImpl<SysLoginInfoMapper, SysLoginInfo> implements ISysLoginInfoService {

}
