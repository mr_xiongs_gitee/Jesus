package com.azr.jesus.service;

import com.azr.jesus.entity.SysMenu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 菜单权限表 服务类
 * @author: Xiong
 * @date: 2022-11-28
 */
public interface ISysMenuService extends IService<SysMenu> {

    List<SysMenu> findMenus(String name);
}
