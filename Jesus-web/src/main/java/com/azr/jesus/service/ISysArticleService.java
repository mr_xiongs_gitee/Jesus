package com.azr.jesus.service;

import com.azr.jesus.entity.SysArticle;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 系统文章表 服务类
 * @author: Xiong
 * @date: 2022-12-19
 */
public interface ISysArticleService extends IService<SysArticle> {

}
