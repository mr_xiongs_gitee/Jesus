package com.azr.jesus.service;

import com.azr.jesus.entity.SysDept;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 部门表 服务类
 * @author: Xiong
 * @date: 2022-12-01
 */
public interface ISysDeptService extends IService<SysDept> {

    List<SysDept> findDeptList(String deptName);

}
