package com.azr.jesus.service;

import com.azr.jesus.entity.SysFiles;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 文件信息表 服务类
 * @author: Xiong
 * @date: 2022-11-20
 */
public interface ISysFileService extends IService<SysFiles> {

}
