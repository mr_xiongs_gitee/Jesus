package com.azr.jesus.service.impl;

import com.azr.jesus.entity.RoleMenu;
import com.azr.jesus.entity.SysRole;
import com.azr.jesus.mapper.RoleMenuMapper;
import com.azr.jesus.mapper.SysRoleMapper;
import com.azr.jesus.service.ISysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 角色信息表 服务实现类
 * @author: Xiong
 * @date: 2022-11-20
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

    @Resource
    private RoleMenuMapper roleMenuMapper;

    @Override
    @Transactional
    public void setRoleMenu(Long roleId, List<Long> menuIds) {
        // 先删除当前角色id所有的绑定关系
        roleMenuMapper.deleteByRoleId(roleId);
        // 再添加
        for (Long menuId : menuIds) {
            RoleMenu roleMenu = new RoleMenu();
            roleMenu.setRoleId(roleId);
            roleMenu.setMenuId(menuId);
            roleMenuMapper.insert(roleMenu);
        }
    }

    @Override
    public List<Long> getRoleMenu(Long roleId) {
        return roleMenuMapper.selectByRoleId(roleId);
    }
}
