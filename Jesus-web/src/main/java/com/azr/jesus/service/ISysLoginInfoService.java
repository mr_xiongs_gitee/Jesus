package com.azr.jesus.service;

import com.azr.jesus.entity.SysLoginInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 系统访问记录 服务类
 * @author: Xiong
 * @date: 2022-12-06
 */
public interface ISysLoginInfoService extends IService<SysLoginInfo> {

}
