package com.azr.jesus.service;

import com.azr.jesus.entity.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 角色信息表 服务类
 * @author: Xiong
 * @date: 2022-11-20
 */
public interface ISysRoleService extends IService<SysRole> {

    void setRoleMenu(Long roleId, List<Long> menuIds);

    List<Long> getRoleMenu(Long roleId);
}
