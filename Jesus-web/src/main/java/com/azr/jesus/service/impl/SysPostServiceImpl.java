package com.azr.jesus.service.impl;

import com.azr.jesus.entity.SysPost;
import com.azr.jesus.mapper.SysPostMapper;
import com.azr.jesus.service.ISysPostService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 岗位信息表 服务实现类
 * @author: Xiong
 * @date: 2022-12-01
 */
@Service
public class SysPostServiceImpl extends ServiceImpl<SysPostMapper, SysPost> implements ISysPostService {

}
