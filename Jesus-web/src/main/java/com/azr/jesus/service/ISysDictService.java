package com.azr.jesus.service;

import com.azr.jesus.entity.SysDictType;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 字典类型表 服务类
 * @author: Xiong
 * @date: 2022-12-01
 */
public interface ISysDictService extends IService<SysDictType> {

}
