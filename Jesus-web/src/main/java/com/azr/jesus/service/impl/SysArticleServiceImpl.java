package com.azr.jesus.service.impl;

import com.azr.jesus.entity.SysArticle;
import com.azr.jesus.mapper.SysArticleMapper;
import com.azr.jesus.service.ISysArticleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 系统文章表 服务实现类
 * @author: Xiong
 * @date: 2022-12-19
 */
@Service
public class SysArticleServiceImpl extends ServiceImpl<SysArticleMapper, SysArticle> implements ISysArticleService {

}
