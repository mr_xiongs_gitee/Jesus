/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 80029
 Source Host           : 127.0.0.1:3307
 Source Schema         : jesus-vue

 Target Server Type    : MySQL
 Target Server Version : 80029
 File Encoding         : 65001

 Date: 01/12/2022 17:21:16
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_article`;
CREATE TABLE `sys_article` (
   `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
   `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '标题',
   `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT '内容',
   `user` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '发布人',
   `create_time` datetime DEFAULT NULL COMMENT '发布时间',
   PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='系统文章表';

DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(0) NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '组级列表',
  `dept_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int(0) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '部门状态（0停用 1启用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 200 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '浩鲸科技', 0, '熊大', '15888888888', 'xiong@163.com', 1, '0', 'admin', '2022-11-12 11:50:44', '', '2022-12-01 14:27:56', NULL);
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '南京总公司', 1, '熊大大', '15888888888', 'xiong@163.com', 1, '0', 'admin', '2022-12-01 14:13:44', '', '2022-12-01 11:21:18', NULL);
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '长沙分公司', 2, '熊大', '15888888888', 'xiong@163.com', 1, '0', 'admin', '2022-11-12 11:50:44', '', '2022-12-01 11:21:21', NULL);
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', '研发部门', 1, '熊大', '15888888888', 'xiong@163.com', 1, '0', 'admin', '2022-11-12 11:50:44', '', '2022-12-01 11:21:18', NULL);
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, '熊二', '15888888888', 'xiong@163.com', 1, '0', 'admin', '2022-11-12 11:50:44', '', '2022-12-01 11:21:19', NULL);
INSERT INTO `sys_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, '熊二', '15888888888', 'xiong@163.com', 1, '0', 'admin', '2022-11-12 11:50:44', '', '2022-12-01 11:21:19', NULL);
INSERT INTO `sys_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, '熊二', '15888888888', 'xiong@163.com', 1, '0', 'admin', '2022-11-12 11:50:44', '', '2022-12-01 11:21:20', NULL);
INSERT INTO `sys_dept` VALUES (107, 101, '0,100,101', '运维部门', 5, '熊二', '15888888888', 'xiong@163.com', 1, '0', 'admin', '2022-11-12 11:50:44', '', '2022-12-01 11:21:20', NULL);
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, '熊二', '15888888888', 'xiong@163.com', 1, '0', 'admin', '2022-11-12 11:50:44', '', '2022-12-01 11:21:22', NULL);
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '财务部门', 2, '熊二', '15888888888', 'xiong@163.com', 1, '0', 'admin', '2022-11-12 11:50:44', '', '2022-12-01 11:21:23', NULL);

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '名称',
  `value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '内容',
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '类型'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '图标' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('user', 'el-icon-user-solid', 'icon');
INSERT INTO `sys_dict` VALUES ('home', 'el-icon-s-home', 'icon');
INSERT INTO `sys_dict` VALUES ('menu', 'el-icon-menu', 'icon');
INSERT INTO `sys_dict` VALUES ('s-custom', 'el-icon-s-custom', 'icon');
INSERT INTO `sys_dict` VALUES ('document', 'el-icon-s-management', 'icon');
INSERT INTO `sys_dict` VALUES ('tools', 'el-icon-s-tools', 'icon');
INSERT INTO `sys_dict` VALUES ('data', 'el-icon-s-data', 'icon');
INSERT INTO `sys_dict` VALUES ('help', 'el-icon-s-help', 'icon');
INSERT INTO `sys_dict` VALUES ('platform', 'el-icon-s-platform', 'icon');
INSERT INTO `sys_dict` VALUES ('cpu', 'el-icon-cpu', 'icon');
INSERT INTO `sys_dict` VALUES ('opportunity', 'el-icon-s-opportunity', 'icon');
INSERT INTO `sys_dict` VALUES ('s-grid', 'el-icon-s-grid', 'icon');
INSERT INTO `sys_dict` VALUES ('document', 'el-icon-document', 'icon');

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(0) NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '状态（0停用 1启用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', 0, 'admin', '2022-11-12 11:50:45', '', NULL, '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', 0, 'admin', '2022-11-12 11:50:45', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', 0, 'admin', '2022-11-12 11:50:45', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', 0, 'admin', '2022-11-12 11:50:45', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', 0, 'admin', '2022-11-12 11:50:45', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', 0, 'admin', '2022-11-12 11:50:45', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', 0, 'admin', '2022-11-12 11:50:45', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', 0, 'admin', '2022-11-12 11:50:45', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', 0, 'admin', '2022-11-12 11:50:45', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', 0, 'admin', '2022-11-12 11:50:45', '', NULL, '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', 0, 'admin', '2022-11-12 11:50:45', '', NULL, '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', 0, 'admin', '2022-11-12 11:50:45', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', 0, 'admin', '2022-11-12 11:50:45', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', 0, 'admin', '2022-11-12 11:50:45', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', 0, 'admin', '2022-11-12 11:50:45', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', 0, 'admin', '2022-11-12 11:50:45', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', 0, 'admin', '2022-11-12 11:50:45', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', 0, 'admin', '2022-11-12 11:50:45', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES (19, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', 0, 'admin', '2022-11-12 11:50:45', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (20, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', 0, 'admin', '2022-11-12 11:50:45', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (21, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', 0, 'admin', '2022-11-12 11:50:45', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (22, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', 0, 'admin', '2022-11-12 11:50:45', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (23, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', 0, 'admin', '2022-11-12 11:50:45', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (24, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', 0, 'admin', '2022-11-12 11:50:45', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (25, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', 0, 'admin', '2022-11-12 11:50:45', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (26, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', 0, 'admin', '2022-11-12 11:50:45', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (27, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', 0, 'admin', '2022-11-12 11:50:45', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (28, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', 0, 'admin', '2022-11-12 11:50:45', '', NULL, '停用状态');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '状态（0停用 1启用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', 1, 'admin', '2022-11-12 11:50:45', '', '2022-12-01 17:14:28', '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', 1, 'admin', '2022-11-12 11:50:45', '', '2022-12-01 17:14:29', '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', 1, 'admin', '2022-11-12 11:50:45', '', '2022-12-01 17:14:29', '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', 1, 'admin', '2022-11-12 11:50:45', '', '2022-12-01 17:14:30', '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', 1, 'admin', '2022-11-12 11:50:45', '', '2022-12-01 17:14:30', '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', 1, 'admin', '2022-11-12 11:50:45', '', '2022-12-01 17:14:30', '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', 1, 'admin', '2022-11-12 11:50:45', '', '2022-12-01 17:14:35', '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', 1, 'admin', '2022-11-12 11:50:45', '', '2022-12-01 17:14:35', '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', 1, 'admin', '2022-11-12 11:50:45', '', '2022-12-01 17:14:35', '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', 1, 'admin', '2022-11-12 11:50:45', '', '2022-12-01 17:14:36', '登录状态列表');

-- ----------------------------
-- Table structure for sys_file
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '文件名称',
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '文件类型',
  `size` bigint(0) NULL DEFAULT NULL COMMENT '文件大小(kb)',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '下载链接',
  `md5` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '文件md5',
  `is_delete` tinyint(1) NULL DEFAULT 0 COMMENT '是否删除',
  `enable` tinyint(1) NULL DEFAULT 1 COMMENT '是否禁用链接',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 61 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_file
-- ----------------------------
INSERT INTO `sys_file` VALUES (1, '报名照片.jpg', 'jpg', 12, 'http://localhost:9876/sys-file/download/b50d61266890472fabff9b7a4a9c24fa.jpg', 'e53c6460bc54b4f4bcde4d5c00576aca', 1, 1);
INSERT INTO `sys_file` VALUES (2, 'b50d61266890472fabff9b7a4a9c24fa.jpg', 'jpg', 12, 'http://localhost:9876/sys-file/download/b50d61266890472fabff9b7a4a9c24fa.jpg', 'e53c6460bc54b4f4bcde4d5c00576aca', 1, 1);
INSERT INTO `sys_file` VALUES (44, '流动人口登记.png', 'png', 552, 'http://localhost:9876/sys-file/download/6b4250a30cee4b9796347c1c8a7b5b05.png', '013adf2824a86ac382ff74187d34bc5c', 1, 1);
INSERT INTO `sys_file` VALUES (45, '证件照.jpg', 'jpg', 25, 'http://localhost:9876/sys-file/download/2387e1f79e414de2bb2fad6fe1301f89.jpg', 'a4b8fc547696f53f4128432e73c1e860', 1, 1);
INSERT INTO `sys_file` VALUES (49, '2387e1f79e414de2bb2fad6fe1301f89.jpg', 'jpg', 25, 'http://localhost:9876/sys-file/download/2387e1f79e414de2bb2fad6fe1301f89.jpg', 'a4b8fc547696f53f4128432e73c1e860', 0, 1);
INSERT INTO `sys_file` VALUES (50, '6b4250a30cee4b9796347c1c8a7b5b05.png', 'png', 552, 'http://localhost:9876/sys-file/download/6b4250a30cee4b9796347c1c8a7b5b05.png', '013adf2824a86ac382ff74187d34bc5c', 0, 1);
INSERT INTO `sys_file` VALUES (51, 'Snipaste_2022-11-23_17-37-55.jpg', 'jpg', 13, 'http://localhost:9876/sys-file/download/c83430f997e44ed3880b560d92a0fb81.jpg', '191abb00c8e2f15bdb2237de4221d8bb', 0, 1);
INSERT INTO `sys_file` VALUES (52, 'Snipaste_2022-11-23_17-37-30.jpg', 'jpg', 14, 'http://localhost:9876/sys-file/download/0e0041dc02cc4fbabce8d5282a90daed.jpg', '1be54165b08f3f3b8fec5b4b8c3cf8e3', 0, 1);
INSERT INTO `sys_file` VALUES (53, 'Snipaste_2022-11-23_17-37-55.jpg', 'jpg', 13, 'http://localhost:9876/sys-file/download/c83430f997e44ed3880b560d92a0fb81.jpg', '191abb00c8e2f15bdb2237de4221d8bb', 1, 1);
INSERT INTO `sys_file` VALUES (54, 'Snipaste_2022-11-23_17-37-30.jpg', 'jpg', 14, 'http://localhost:9876/sys-file/download/0e0041dc02cc4fbabce8d5282a90daed.jpg', '1be54165b08f3f3b8fec5b4b8c3cf8e3', 0, 1);
INSERT INTO `sys_file` VALUES (55, 'Snipaste_2022-11-23_17-37-55.jpg', 'jpg', 13, 'http://localhost:9876/sys-file/download/c83430f997e44ed3880b560d92a0fb81.jpg', '191abb00c8e2f15bdb2237de4221d8bb', 1, 1);
INSERT INTO `sys_file` VALUES (56, 'Snipaste_2022-11-23_17-37-30.jpg', 'jpg', 14, 'http://localhost:9876/sys-file/download/0e0041dc02cc4fbabce8d5282a90daed.jpg', '1be54165b08f3f3b8fec5b4b8c3cf8e3', 0, 1);
INSERT INTO `sys_file` VALUES (57, 'Snipaste_2022-11-23_17-37-55.jpg', 'jpg', 13, 'http://localhost:9876/sys-file/download/c83430f997e44ed3880b560d92a0fb81.jpg', '191abb00c8e2f15bdb2237de4221d8bb', 0, 1);
INSERT INTO `sys_file` VALUES (58, 'Snipaste_2022-11-23_17-37-30.jpg', 'jpg', 14, 'http://localhost:9876/sys-file/download/0e0041dc02cc4fbabce8d5282a90daed.jpg', '1be54165b08f3f3b8fec5b4b8c3cf8e3', 0, 1);
INSERT INTO `sys_file` VALUES (59, 'Snipaste_2022-11-23_17-37-30.jpg', 'jpg', 14, 'http://localhost:9876/sys-file/download/0e0041dc02cc4fbabce8d5282a90daed.jpg', '1be54165b08f3f3b8fec5b4b8c3cf8e3', 0, 1);
INSERT INTO `sys_file` VALUES (60, 'Snipaste_2022-11-23_17-37-55.jpg', 'jpg', 13, 'http://localhost:9876/sys-file/download/c83430f997e44ed3880b560d92a0fb81.jpg', '191abb00c8e2f15bdb2237de4221d8bb', 0, 1);

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(0) NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(0) NULL DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '页面路径',
  `query` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '路由参数',
  `is_frame` int(0) NULL DEFAULT 1 COMMENT '是否为外链（0是 1否）',
  `is_cache` int(0) NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '菜单状态（0停用 1启用）',
  `perms` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2005 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '首页', 0, 1, '/home', 'Home', NULL, 1, 0, 'M', '0', 1, NULL, 'el-icon-s-home', '', '2022-11-29 20:40:31', '', '2022-11-29 20:35:01', '首页');
INSERT INTO `sys_menu` VALUES (2, '系统管理', 0, 2, '', NULL, '', 1, 0, 'M', '0', 1, '', 'el-icon-s-grid', 'admin', '2022-11-29 20:40:36', '', '2022-11-28 15:52:02', '系统管理目录');
INSERT INTO `sys_menu` VALUES (3, '系统监控', 0, 3, '', NULL, '', 1, 0, 'M', '0', 1, '', 'el-icon-s-tools', 'admin', '2022-11-28 15:59:40', '', '2022-11-28 15:52:06', '系统监控目录');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 2, 1, '/user', 'User', '', 1, 0, 'C', '0', 1, 'system:user:list', 'el-icon-user-solid', 'admin', '2022-11-28 16:03:51', '', '2022-11-28 15:52:02', '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 2, 2, '/sys-role', 'Role', '', 1, 0, 'C', '0', 1, 'system:role:list', 'el-icon-s-custom', 'admin', '2022-11-28 15:54:01', '', '2022-11-28 15:52:03', '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 2, 3, '/sys-menu', 'Menu', '', 1, 0, 'C', '0', 1, 'system:menu:list', 'el-icon-menu', 'admin', '2022-11-28 15:58:03', '', '2022-11-28 15:52:03', '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 2, 4, '/sys-dept', 'Department', '', 1, 0, 'C', '0', 1, 'system:dept:list', 'el-icon-s-platform', 'admin', '2022-11-28 16:03:05', '', '2022-11-28 15:52:04', '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 2, 5, '/sys-post', 'Post', '', 1, 0, 'C', '0', 1, 'system:post:list', 'el-icon-s-help', 'admin', '2022-11-28 16:03:00', '', '2022-11-28 15:52:04', '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 2, 6, '/sys-dict', 'Dictionary', '', 1, 0, 'C', '0', 1, 'system:dict:list', 'el-icon-s-management', 'admin', '2022-11-28 16:04:43', '', '2022-11-28 15:52:05', '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '文件管理', 2, 7, '/sys-file', 'File', NULL, 1, 0, 'C', '0', 1, NULL, 'el-icon-document', '', '2022-11-29 21:42:55', '', '2022-11-29 21:41:12', '文件管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 3, 1, '/sys-online', 'Online', '', 1, 0, 'C', '0', 1, 'monitor:online:list', 'el-icon-user-solid', 'admin', '2022-11-28 16:04:52', '', '2022-11-28 15:52:08', '在线用户菜单');
INSERT INTO `sys_menu` VALUES (111, '数据监控', 3, 3, '/sys-druid', 'Druid', '', 1, 0, 'C', '0', 1, 'monitor:druid:list', 'el-icon-s-data', 'admin', '2022-11-29 19:21:47', '', '2022-11-28 15:52:10', '数据监控菜单');
INSERT INTO `sys_menu` VALUES (112, '服务监控', 3, 4, '/sys-server', 'Server', '', 1, 0, 'C', '0', 1, 'monitor:server:list', 'el-icon-cpu', 'admin', '2022-11-29 19:21:53', '', '2022-11-28 15:52:11', '服务监控菜单');
INSERT INTO `sys_menu` VALUES (113, '缓存监控', 3, 5, '/sys-cache', 'Cache', '', 1, 0, 'C', '0', 1, 'monitor:cache:list', 'el-icon-s-opportunity', 'admin', '2022-11-29 19:21:58', '', '2022-11-28 15:52:11', '缓存监控菜单');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '岗位名称',
  `order_num` int(0) NOT NULL COMMENT '显示顺序',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态（0停用 1启用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '岗位信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, 1, 'admin', '2022-11-12 11:50:44', '', '2022-12-01 15:03:40', '');
INSERT INTO `sys_post` VALUES (2, 'se', '项目经理', 2, 1, 'admin', '2022-11-12 11:50:44', '', '2022-12-01 15:03:41', '');
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', 3, 1, 'admin', '2022-11-12 11:50:44', '', '2022-12-01 15:03:41', '');
INSERT INTO `sys_post` VALUES (4, 'user', '普通员工', 4, 1, 'admin', '2022-11-12 11:50:44', '', '2022-12-01 15:03:42', '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(0) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '部门树选择项是否关联显示',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '角色状态（0停用 1启用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 102 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'ROLE_ADMIN', 1, '1', 1, 1, 1, '0', 'admin', '2022-11-28 11:13:04', '', NULL, '超级管理员');
INSERT INTO `sys_role` VALUES (2, '普通角色', 'ROLE_USER', 2, '2', 1, 1, 1, '0', 'admin', '2022-11-28 11:13:08', '', NULL, '普通角色');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint(0) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(0) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色和部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (2, 100);
INSERT INTO `sys_role_dept` VALUES (2, 101);
INSERT INTO `sys_role_dept` VALUES (2, 105);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint(0) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(0) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (1, 1);
INSERT INTO `sys_role_menu` VALUES (1, 2);
INSERT INTO `sys_role_menu` VALUES (1, 3);
INSERT INTO `sys_role_menu` VALUES (1, 100);
INSERT INTO `sys_role_menu` VALUES (1, 101);
INSERT INTO `sys_role_menu` VALUES (1, 102);
INSERT INTO `sys_role_menu` VALUES (1, 103);
INSERT INTO `sys_role_menu` VALUES (1, 104);
INSERT INTO `sys_role_menu` VALUES (1, 105);
INSERT INTO `sys_role_menu` VALUES (1, 106);
INSERT INTO `sys_role_menu` VALUES (1, 109);
INSERT INTO `sys_role_menu` VALUES (1, 111);
INSERT INTO `sys_role_menu` VALUES (1, 112);
INSERT INTO `sys_role_menu` VALUES (1, 113);
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (2, 100);
INSERT INTO `sys_role_menu` VALUES (2, 101);
INSERT INTO `sys_role_menu` VALUES (2, 102);
INSERT INTO `sys_role_menu` VALUES (2, 103);
INSERT INTO `sys_role_menu` VALUES (2, 106);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(0) NULL DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '密码',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '帐号状态（0停用 1启用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 103 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 103, 'admin', '姜承録', '00', 'jcl@163.com', '15888888888', '0', 'http://localhost:9876/sys-file/download/0e0041dc02cc4fbabce8d5282a90daed.jpg', '21232f297a57a5a743894a0e4a801fc3', 0, '0', '127.0.0.1', '2022-11-12 11:50:44', 'admin', '2022-11-12 11:50:44', '', NULL, '管理员');
INSERT INTO `sys_user` VALUES (2, 105, 'root', '狂小狗', '00', 'kxg@qq.com', '15666666666', '0', 'http://localhost:9876/sys-file/download/c83430f997e44ed3880b560d92a0fb81.jpg', 'e10adc3949ba59abbe56e057f20f883e', 0, '0', '127.0.0.1', '2022-11-12 11:50:44', 'admin', '2022-11-30 10:35:35', '', NULL, '测试员');
INSERT INTO `sys_user` VALUES (100, 103, 'test', '庆成', '00', 'qch@163.com', '15888889999', '0', '', 'e10adc3949ba59abbe56e057f20f883e', 0, '0', '', NULL, '', '2022-11-28 16:21:45', '', NULL, '测试');
INSERT INTO `sys_user` VALUES (101, 105, 'test', '泱泱', '00', 'yy@qq.com', '19866666666', '1', '', '', 0, '0', '', NULL, '', '2022-11-28 16:21:49', '', NULL, NULL);
INSERT INTO `sys_user` VALUES (102, 106, 'test', '李叔同', '00', 'lshut@qq.com', '19866669999', '0', '', '', 0, '0', '', NULL, '', '2022-11-28 16:21:52', '', NULL, '都能看到看静安寺');

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint(0) NOT NULL COMMENT '用户ID',
  `post_id` bigint(0) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);
INSERT INTO `sys_user_post` VALUES (2, 2);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint(0) NOT NULL COMMENT '用户ID',
  `role_id` bigint(0) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 2);
INSERT INTO `sys_user_role` VALUES (100, 2);
INSERT INTO `sys_user_role` VALUES (101, 2);
INSERT INTO `sys_user_role` VALUES (102, 2);

SET FOREIGN_KEY_CHECKS = 1;
